/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import util.MyDate;
import util.StringFormatException;

/**
 * The extended version of {@link CommunicationHelper} specially
 * designed for the client side communication string operations.
 * <p>
 * It adds creation of client requests string and client response parsing.
 * @author  sahil
 */
public class ClientCommunicationHelper extends CommunicationHelper{
    
    public static String createLoginString(String username, String password){
        return LOGIN + MAIN_SEPARATOR + username + DELIMITER + password;
    }
    public static String createSignupString(String username, String password){
        return SIGNUP + MAIN_SEPARATOR + username + DELIMITER + password;
    }
    public static String createGetUserStatusString(String username){
        return USER_STATUS + MAIN_SEPARATOR + username;
    }
    public static String createGetLastSeenString(String username){
        return LAST_SEEN + MAIN_SEPARATOR + username;
    }
    
    public boolean readStringIntBool(String bool){
        return "1".equals(bool);
    }
    public static boolean parseLoginStatus(String input){
        if(input.equals("1")){
            return true;
        }else if(input.equals("0")){
            return false;
        }else{
            throw new StringFormatException("Invalid Login String Format");
        }
    }
    public static boolean parseSignupStatus(String input){
        if(input.equals("1")){
            return true;
        }else if(input.equals("0")){
            return false;
        }else{
            throw new StringFormatException("Invalid Signup String Format");
        }
    }
    public static Map<String, Boolean> parseUserList(String input){
        String[] users = input.split(GROUP_SEPERATOR);
        HashMap<String, Boolean> map = new HashMap();
        for (String user : users) {
            int partitionInd = user.indexOf(DELIMITER);
            String username = user.substring(0, partitionInd);
            Boolean online = "1".equals(user.substring(partitionInd+1));
            map.put(username, online);
        }
        return map;
    }
    
    public static String parseLastSeenValue(String input){
        return ("-1".equals(input)) ? "online" : MyDate.getLastSeen(new Date(), new Date(new Long(input)));
    }
}
