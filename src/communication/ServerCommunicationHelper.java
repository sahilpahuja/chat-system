/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

import communication.CommunicationHelper;
import entity.User;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author sahil
 */
public class ServerCommunicationHelper extends CommunicationHelper{
    
    public static String createLoginResultString(boolean loginSuccessful){
        return LOGIN + MAIN_SEPARATOR + (loginSuccessful?"1":"0");
    }
    public static String createSignupResultString(boolean signupSuccessful){
        return SIGNUP + MAIN_SEPARATOR + (signupSuccessful?"1":"0");
    }
    
    public static String createUserString(String username, Long lastOnline){
        return USER_LIST + MAIN_SEPARATOR + (username + ":" + (lastOnline==-1?"1":"0"));
    }
    
    public static String createUsersString(Map<String, Long> users, String exceptUsername){
        String usersString = null;
        for (Map.Entry user : users.entrySet()) {
            if (user.getKey().equals(exceptUsername)) {
                continue;
            }
            if(usersString == null){
                usersString = user.getKey() + ":" + ((Long)user.getValue()==-1?"1":"0");
            }else{
                usersString += GROUP_SEPERATOR + user.getKey() + ":" + ((Long)user.getValue()==-1?"1":"0");
            }
        }
        return USER_LIST+ MAIN_SEPARATOR + (usersString);
    }
    
    public static String createUserOnlineStatusString(String username, boolean isOnline){
        return USER_STATUS + MAIN_SEPARATOR + username + DELIMITER + (isOnline?"1":"0");
    }
    
    public static String createLastSeenString(String username, Long lastSeen){
        return LAST_SEEN + MAIN_SEPARATOR + username + DELIMITER + lastSeen;
    }
}
