/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

/**
 *
 * @author sahil
 */
public class Acknowledge {
    // status for sending Ack from server to client-1
    public static final int SERVER_RECEIVED = 1;
    
    // status for sending Ack from server to client-1
    public static final int RECEIVER_DELIVERED = 2;
    public static final int RECEIVER_SEEN = 3;
    
    // status for sending Ack from client-2 to server
    public static final int CLIENT_RECEIVED = 4;
    public static final int CLIENT_SEEN = 5;
    
    public final String sender;
    public final String receiver;
    public final int ackType;
    public final long creationTimeInMS;

    public Acknowledge(int ackType, String sender, String receiver, long creationTimeInMS) {
        this.sender = sender;
        this.receiver = receiver;
        this.ackType = ackType;
        this.creationTimeInMS = creationTimeInMS;
    }
}
