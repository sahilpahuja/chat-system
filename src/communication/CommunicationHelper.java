/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

import entity.User;
import util.MessageType;

/**
 * A simple utility class which can help to communicate a client and a server
 * in context of chat application.
 * 
 * <p>
 * This class is useful to communicate between non-object type
 * to append different types of request and response in a single string
 * by non-used character to identify different part of string on client and server side.
 * For example, if you want to send the login request in string format: 
 * 
 * <blockquote><pre>
 *     n ~ username : password
 * </pre></blockquote><p>
 * Here n is number constant defied by {@link MessageType}.
 * <p> {@value #MAIN_SEPARATOR} is main separator which separates message type and message content.
 *
 * <p> This class contains the utility methods to manipulate string values into useful outputs easily.
 *
 * @author sahil
 */
public class CommunicationHelper implements MessageType{
    
    /**
     * The communication string contains a separator
     * to separate two parts, message type and message value.
     */
    public static final String MAIN_SEPARATOR = "~";
    
    /**
     * The communication string  contains a separator to
     * separate key-value pair in message part.
     */
    public static final String DELIMITER = ":";
    
    /**
     * The communication string can contain multiple key-value
     * pairs separated by delimiter.
     */
    public static final String GROUP_SEPERATOR = ",";
    
    /**
     * Returns true if and only if specified string contains 
     * the specified {@value #MAIN_SEPARATOR}.
     *
     * @param str the string to be checked
     * @return {@code true} if {@code str} contains {@value #MAIN_SEPARATOR}, false otherwise
     */
    public static boolean containsMainSeprator(String str){
        return str.contains(MAIN_SEPARATOR);
    }
    
    /**
     * Remove message type from the communication string
     * with the separator used.
     * <p>
     * If the string does not contains the main separator then
     * same string will be returned.
     * 
     * @param str the communication string from which type part will be removed.
     * @return the content part of string.
     */
    public static String removeMessageType(String str){
        if(str.contains(MAIN_SEPARATOR)){
            return str.substring(str.indexOf(MAIN_SEPARATOR)+1);
        }
        return str;
    }
    
    public static String getLastSepration(String str){
        if(str.contains(DELIMITER)){
            return str.substring(0, str.lastIndexOf(DELIMITER));
        }
        return str;
    }
    public static String removeLastSepration(String str){
        if(str.contains(DELIMITER)){
            return str.substring(0, str.lastIndexOf(DELIMITER));
        }
        return str;
    }
    /**
     * Determines the message type of communication string.
     * @param str is the communication string which contains message type.
     * @return the {@code int} type of the string. If the valid type is not found {@code -1} is returned.
     */
    public static int parseMessageType(String str){
        String type = str;
        
        if(str.contains(MAIN_SEPARATOR)){
            type = str.substring(0, str.indexOf(MAIN_SEPARATOR));
        }
        
        return isValidType(type);
    }
    
    /**
     * Determines if the {@code messageType} is valid message type.
     * @param messageType is the message type to be determined.
     * @return {@code messageType} if the type is valid, otherwise {@code -1}.
     */
    public static int isValidType(String messageType){
        int type = Integer.parseInt(messageType);
        if (type == MESSAGE || type == ACK || type == USER_LIST || type == USER_STATUS || type == LOGIN || type == SIGNUP || type == LAST_SEEN){
            return type;
        }
        return -1;
    }
    
    public static User parseUserString(String user) {
        String arr[] = breakInStrings(user, 2);
        return new User(arr[0], arr[1], 0);
    }
    
    public static String createMessageString(Message message){
        return MESSAGE + MAIN_SEPARATOR + message.sender + DELIMITER + message.content + DELIMITER + message.receiver + DELIMITER + message.creationTimeInMS;
    }
    
    public static Message parseMessageString(String message){
        String arr[] = breakInStrings(message, 4);
        
        return new Message(arr[0], arr[1], arr[2], Long.valueOf(arr[3]));
    }
    
    public static String createAckString(Acknowledge acknowledge){
        return ACK + MAIN_SEPARATOR + acknowledge.ackType + DELIMITER + acknowledge.sender + DELIMITER + acknowledge.receiver + DELIMITER + acknowledge.creationTimeInMS;
    }
    
    public static Acknowledge parseAckString(String acknowledge){
        String arr[] = breakInStrings(acknowledge, 4);
        
        return new Acknowledge(Integer.parseInt(arr[0]), arr[1], arr[2], Long.valueOf(arr[3]));
    }
    
    public static String[] breakInStrings(String string, int partitions){
        String arr[] = new String[partitions];
        
        for(int i=0, lastFound=0; i<arr.length; i++){
            if(i==0){
                lastFound = string.indexOf(DELIMITER, 0);
                arr[i] = string.substring(0, lastFound);
            }else if(i==arr.length-1){
                arr[i] = string.substring(lastFound+1);
            }else{
                arr[i] = string.substring(lastFound+1, lastFound = string.indexOf(DELIMITER, lastFound+1));
            }
        }
        
        return arr;
    }
}
