/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

/**
 *
 * @author sahil
 */
public class Message {
    public final String sender;
    public final String content;
    public final String receiver;
    public final long creationTimeInMS;

    public Message(String sender, String content, String receiver, long creationTimeInMS) {
        this.sender = sender;
        this.content = content;
        this.receiver = receiver;
        this.creationTimeInMS = creationTimeInMS;
    }
    
}
