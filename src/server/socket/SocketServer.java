/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.socket;
import communication.Acknowledge;
import entity.User;
import static communication.CommunicationHelper.parseMessageType;
import static communication.CommunicationHelper.removeMessageType;
import communication.ServerCommunicationHelper;
import static communication.ServerCommunicationHelper.createSignupResultString;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import communication.Message;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.ServerCode;
import server.sql.MySQLConnect;

/**
 *
 * @author sahil
 */
public class SocketServer extends ServerCommunicationHelper {
    private final Connection conn;
    Socket socket;
    String username;
    BufferedReader input;
    PrintWriter output;
    private Reader inputReader;
    private ServerCode parent;
    private Map<String, Long> userList;
    public SocketServer(ServerCode serverCode, Socket socket, Map<String, Long> users) throws IOException {
        parent = serverCode;
        userList = users;
        conn = MySQLConnect.getConnection();
        initStreams(socket);
//        this.serverAddress = socket.getInetAddress();
//        messageQueue = new CustomQueue<>(output, serverAddress, this);
        startInputRead();
    }
    private void initStreams(Socket socket) throws IOException {
        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        output = new PrintWriter(socket.getOutputStream(), true);
    }
    private void startInputRead() {
        inputReader = new Reader();
        inputReader.startReading();
    }
    
    public String getUsername()
    {
        return this.username;
    }
    
    private void broadcastUserList()
    {
        output.println(createUsersString(userList, this.username));
    }
    
    public void broadcastNewUser(String username, long lastOnline)
    {
        if (!username.equals(this.username)) {
            output.println(createUserString(username, lastOnline));
        }
    }

    private boolean login(User user) {
        boolean loginSuccessful = false;
        try {
            PreparedStatement ps = conn.prepareStatement("SELECT username FROM users WHERE username = ? AND password = ?");
            ps.setString(1, user.username);
            ps.setString(2, user.password);
            loginSuccessful = (ps.executeQuery()).next();
        } catch (SQLException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (loginSuccessful) {
            this.username = user.username;
        }
        return loginSuccessful;
    }
    
    private boolean signUp(User user) {
        return parent.signUp(user);
    }
    
    public void sendUserOnlineStatus(String username, boolean isOnline) {
        output.println(createUserOnlineStatusString(username, isOnline));
    }
    
    public void transmitMessage(Message m) {
        output.println(createMessageString(new Message(m.sender, m.content, m.receiver, System.currentTimeMillis())));
    }
    
    public void transmitAck(Acknowledge ack) {
        output.println(createAckString(ack));
    }
    
    private void clientOffline() {
        parent.logoutUser(this, this.username);
    }
    
    private SocketServer getParent() {
        return this;
    }
    
    private class Reader implements Runnable{
        private volatile boolean status;
        private final Thread reader;
        private int idleSecondsCounter;
        public Reader() {
            reader = new Thread(this);
        }
        private void checkHeartBeat() {
            output.println(HEARTBEAT);
        }
        @Override
        public void run(){
            try{
                while(status){
                    while(!input.ready() && status){
                        ++idleSecondsCounter;
                        Thread.sleep(500);
                        if (idleSecondsCounter > 9) {
                            // Checking heartbeat of user every 15 seconds
                            if (idleSecondsCounter == 30) {
                                checkHeartBeat();
                            }
                            // After 5 seconds of checking, if pulse not detected then terminate the thread
                            else if (idleSecondsCounter > 40) {
                                stopReading();
                            }
                        }
                    }                   
                    idleSecondsCounter = 0;
                    if(status){
                        String incomingPacket = input.readLine();
                        if (incomingPacket == null) {
                            System.out.println("PACKET ON SERVER: " + incomingPacket);
                        }
                        if ((HEARTBEAT+"").equals(incomingPacket)) {
                            continue;
                        }
                        
                        int messageType = parseMessageType(incomingPacket);
                        String messageWithoutType = removeMessageType(incomingPacket);
                        switch(messageType){
                            case ServerCommunicationHelper.MESSAGE:
                            {
                                Message m = parseMessageString(messageWithoutType);
                                
                                // Sending Acknowledge to sender that message is received till server
                                String ack = ServerCommunicationHelper.createAckString(new Acknowledge(Acknowledge.SERVER_RECEIVED, m.sender, m.receiver, m.creationTimeInMS));
                                System.out.println("+++++CREATING and sending ack to client back ========");
                                output.println(ack);
                                
                                parent.sendMessage(m);
                                break;
                            }
                            case ServerCommunicationHelper.LOGIN:
                                User user = parseUserString(messageWithoutType);
                                boolean loginSuccess = login(user);
                                output.println(createLoginResultString(loginSuccess));
                                if (loginSuccess) {
                                    broadcastUserList();
                                    parent.loginUser(getParent(), username);
                                }
                                break;
                            case ServerCommunicationHelper.SIGNUP:
                                user = parseUserString(messageWithoutType);
                                output.println(createSignupResultString(signUp(user)));
                                break;
                            case ServerCommunicationHelper.USER_LIST:
                                // NOT IN USE
                                break;
                            case ServerCommunicationHelper.LAST_SEEN:
                                Long lastSeen = userList.get(messageWithoutType);
                                output.println(createLastSeenString(messageWithoutType, lastSeen));
                                break;
                            case ServerCommunicationHelper.ACK:
                                Acknowledge ack = parseAckString(messageWithoutType);
                                parent.sendAck(ack);
                                break;
                        }
                    }
                }
            }catch(IOException e){
                System.out.println("Exception while reading input from Client: "+e);
            } catch (InterruptedException ex) {
                Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                clientOffline();
            }
        }
        
        public void startReading(){
            status = true;
            reader.start();
        }
        
        public void stopReading(){
            status = false;
        }
    }
}
