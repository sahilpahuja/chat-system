/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import communication.Acknowledge;
import communication.Message;
import entity.User;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.socket.SocketServer;
import server.sql.MySQLConnect;

/**
 *
 * @author sahil
 */
public class ServerCode {
    private final Connection conn;
    private final UsersMap<String, Long> users;
    private final Map<String, SocketServer> sockets;

    public ServerCode() {
        conn = MySQLConnect.getConnection();
        users = new UsersMap<>();
        sockets = new ConcurrentHashMap<>();
        loadUsers();
    }

    private void initDB() {
        try {
            conn.createStatement().execute("CREATE TABLE IF NOT EXISTS users (username varchar(32) PRIMARY KEY NOT NULL, password varchar(255) NOT NULL, last_online varchar(13), creation_time_in_ms varchar(13) NOT NULL)");
            conn.createStatement().execute(
                "CREATE TABLE IF NOT EXISTS messages " +
                    "(id int unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT, " +
                    "message TEXT NOT NULL, " +
                    "sender varchar(32), " +
                    "receiver varchar(32), " +
                    "message_time varchar(13) NOT NULL," +
                    "FOREIGN KEY (sender) REFERENCES users(username), " +
                    "FOREIGN KEY (receiver) REFERENCES users(username)" +
                ")");
            conn.createStatement().execute(
                "CREATE TABLE IF NOT EXISTS acknowledgments " +
                    "(id int unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT, " +
                    "ack_type INTEGER NOT NULL, " +
                    "sender varchar(32), " +
                    "receiver varchar(32), " +
                    "creation_time varchar(13) NOT NULL," +
                    "FOREIGN KEY (sender) REFERENCES users(username), " +
                    "FOREIGN KEY (receiver) REFERENCES users(username)" +
                ")");
            //UNIX_TIMESTAMP());
        } catch (SQLException ex) {
            Logger.getLogger(ServerCode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadUsers()
    {
        initDB();
        try{
            ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM users");
            while (rs.next()) {
                String username = rs.getString("username");
                System.out.println(rs.getString("last_online"));
                Long lastOnline = Long.parseLong(rs.getString("last_online"));
                users.put(username, lastOnline);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServerCode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void loginUser(SocketServer socketServer, String username) {
        // Making user online
        users.replace(username, -1L);
        broadUserOnlineStatus(username, true);
        // Adding loggedin Socket
        sockets.put(username, socketServer);
        sendPendingMessages(username, socketServer);
    }
    
    public void logoutUser(SocketServer socketServer, String username) {
        /** If user logged out. And the logout function is not performed on the server yet
          * In between the user logged in again (with another socket).
          * Then the in process logout for previous socket will be performed and the user will be marked as offline
          * To solved this problem checking if the socket is changed before performing logout operation
        */
        if(sockets.get(username) != socketServer) {
            return;
        }
        
        // Making user offline
        Long logoutTime = System.currentTimeMillis();
        users.replace(username, logoutTime);
        broadUserOnlineStatus(username, false);
        // Changin lastSeen in database
        changeLastSeenInDB(username, logoutTime);
        // Removing Socket
        sockets.remove(username);
    }
    
    public boolean signUp(User user) {
        long currentTimeInMs = System.currentTimeMillis();
        try {
            PreparedStatement ps = conn.prepareStatement("INSERT INTO users (username, password, last_online, creation_time_in_ms) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.username);
            ps.setString(2, user.password);
            ps.setLong(3, currentTimeInMs);
            ps.setLong(4, currentTimeInMs);
            boolean success = ps.executeUpdate() > 0;
            if (success) {
                users.put(user.username, currentTimeInMs);
                broadcaseNewUser(user.username, currentTimeInMs);
            }
            return success;
        } catch (SQLException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public void sendMessage(Message m) {
        // Sending message to receiver client
        SocketServer clientSocket;
        if (users.get(m.receiver) == -1
            && (clientSocket = ((SocketServer)(sockets.get(m.receiver)))) != null){
            clientSocket.transmitMessage(m);
            return;
        }
        // If unable to send message then store in DB
        try {
            PreparedStatement ps = conn.prepareStatement("INSERT INTO messages (message, sender, receiver, message_time) VALUES (?, ?, ?, ?)");
            ps.setString(1, m.content);
            ps.setString(2, m.sender);
            ps.setString(3, m.receiver);
            ps.setLong(4, m.creationTimeInMS);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void sendAck(Acknowledge ack) {
        int ackType = -1;
        // Changing ACK intended for Server to ACK intended for message sender client
        if (ack.ackType == Acknowledge.CLIENT_RECEIVED) {
            ackType = Acknowledge.RECEIVER_DELIVERED;
        } else if (ack.ackType == Acknowledge.CLIENT_SEEN) {
            ackType = Acknowledge.RECEIVER_SEEN;
        }
        ack = new Acknowledge(ackType, ack.sender, ack.receiver, ack.creationTimeInMS);
        
        SocketServer clientSocket;
        if (users.get(ack.receiver) == -1
            && (clientSocket = ((SocketServer)(sockets.get(ack.receiver)))) != null){
            clientSocket.transmitAck(ack);
            return;
        }
        
        // If unable to send acknowledge then store in DB
        try {
            PreparedStatement ps = conn.prepareStatement("INSERT INTO acknowledgments (ack_type, sender, receiver, creation_time) VALUES (?, ?, ?, ?)");
            ps.setInt(1, ack.ackType);
            ps.setString(2, ack.sender);
            ps.setString(3, ack.receiver);
            ps.setLong(4, ack.creationTimeInMS);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void changeLastSeenInDB(String username, Long lastSeen) {
        try {
            conn.createStatement().executeUpdate("UPDATE users SET last_online = " + lastSeen + " WHERE username = '" + username + "'");
        } catch (SQLException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        ServerCode serverCode = new ServerCode();
    	try(ServerSocket serverSocket = new ServerSocket(5000)){
            while (true) {
                Socket socket = serverSocket.accept();
                new SocketServer(serverCode, socket, serverCode.users);
            }
    	}catch(IOException e){
            System.out.println("Server Exception: " + e);
    	}
    }
    
    // Will be used at the time of signup
    private void broadcaseNewUser(String username, Long lastOnline) {
        for (Map.Entry<String, SocketServer> entrySet : sockets.entrySet()) {
            SocketServer currentUserSocket = entrySet.getValue();            
            currentUserSocket.broadcastNewUser(username, lastOnline);
        }
    }
    
    private void broadUserOnlineStatus(String username, boolean isOnline) {
        for (Map.Entry<String, SocketServer> entrySet : sockets.entrySet()) {
            if(username.equals(entrySet.getKey())) continue;
            
            SocketServer currentUserSocket = entrySet.getValue();
            currentUserSocket.sendUserOnlineStatus(username, isOnline);
        }
    }
    
    private void sendPendingMessages(String username, SocketServer socketServer) {
        try{
            ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM messages WHERE receiver = " + "'" + username + "'");
            while (rs.next()) {
                String content = rs.getString("message");
                String sender = rs.getString("sender");
                Long messageTime = Long.parseLong(rs.getString("message_time"));
                socketServer.transmitMessage(new Message(sender, content, username, messageTime));
            }
            conn.createStatement().executeUpdate("DELETE FROM messages WHERE receiver = " + "'" + username + "'");
        } catch (SQLException ex) {
            Logger.getLogger(ServerCode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
//    private void broadcastUserStatus(String username, Long lastOnline)
//    {
//        for (Map.Entry<String, SocketServer> entrySet : sockets.entrySet()) {
//            String currentUserUsername = entrySet.getKey();
//            SocketServer currentUserSocket = entrySet.getValue();
//            
//            if (!currentUserUsername.equals(username)) {
//                currentUserSocket.sendUserStatus(currentUserUsername, lastOnline);
//            }
//        }
//    }
    
    private static class UsersMap<String, Long> extends ConcurrentHashMap<String, Long>
    {
        @Override
        public Long put(String username, Long lastOnline) {
            return super.put(username, lastOnline);
        }

        @Override
        public Long replace(String username, Long lastOnline) {
//            broadcastUserStatus(username, lastOnline);
            return super.replace(username, lastOnline);
        }
    }
}
