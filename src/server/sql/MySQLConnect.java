/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.sql;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author sahil
 */
public class MySQLConnect {
    private static final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/chat_system";
    private static final String USERNAME = "sahil";
    private static final String PASSWORD = "sahil";
  /**
 * 
 * @return Connection
 * Returns a valid connection to specified DB if it can, otherwise it returns null
 */
  public static Connection getConnection(){
      Connection conn = null;
      try{
          conn = DriverManager.getConnection(CONNECTION_STRING, USERNAME, PASSWORD);
      }catch(SQLException ex){
          JOptionPane.showMessageDialog(null, "Connection Failed: " + ex.getMessage());
      }
      return conn;
  }
}