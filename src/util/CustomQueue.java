
package util;
import client.socket.SocketClient;
import java.util.LinkedList;
import java.io.PrintWriter;
import java.io.IOException;
import java.net.InetAddress;

/**
 *
 * @author sahil
 */

public class CustomQueue<E> implements Runnable{
	private final LinkedList<E> elements;
	private final Thread messageSendingThread;
	private final PrintWriter output;
	private final InetAddress serverAddress;
        private final SocketClient socketClient;
	private boolean flag;
	public CustomQueue(PrintWriter output, InetAddress serverAddress, SocketClient socketClient){
            messageSendingThread = new Thread(this);
            this.output = output;
            this.serverAddress = serverAddress;
            this.socketClient = socketClient;
            elements = new LinkedList<>();
	}
	public void addFirst(E element){
            synchronized(elements){
                elements.addFirst(element);
                if(!messageSendingThread.isAlive()){
                        messageSendingThread.start();
                }
                socketClient.setConnection(false);
            }
	}
	public void add(E element){
            synchronized(elements){
                elements.add(element);
                if(!messageSendingThread.isAlive()){
                        messageSendingThread.start();
                }
                socketClient.setConnection(false);
            }
	}
	public E take(){
            synchronized(elements){
                    E e = null;
                    if((e =elements.poll()) == null){
                            try{
                                    System.out.println("waiting!");
                                    elements.wait();
                                    e = elements.remove();
                            }catch(InterruptedException ex){System.out.println("Exception: " + ex);}

                    }
                    return e;
            }
	}

	@Override
	public void run(){
            String message = null;
            while((message = (String)this.elements.poll()) != null){
                output.println(message);
                try{
                    if(!(serverAddress.isReachable(10000))){
                            flag = true;
                            socketClient.setConnection(false);
                            this.addFirst((E)message);
                            while(!(serverAddress.isReachable(1000))){
                                    try{
                                            Thread.sleep(3000);
                                            System.out.println("Checking If internet is available after 3 secs!");
                                    }catch(InterruptedException ex){System.out.println("Exception: " + ex);}
                            }
                            flag = false;
                            socketClient.setConnection(true);
                    }
                }catch(IOException ex){System.out.println("Exception: " + ex);}
            }
	}

	public boolean isRunning(){
            if(!flag){
                while(messageSendingThread.isAlive());
            }
            return this.flag;
	}
}