/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 * This interface contains the constants to identify string message type
 * passed in non-object streams for chat application.
 *
 * @author  sahil
 */
public interface MessageType {
    
    public static final int LOGIN = 1;
    public static final int SIGNUP = 2;
    public static final int USER_LIST = 3;
    public static final int MESSAGE = 4;
    public static final int USER_STATUS = 5;
    public static final int LAST_SEEN = 6;
    public static final int ACK = 7;
    public static final int HEARTBEAT = 8;
    public static final int LOGOUT = 10;
}
