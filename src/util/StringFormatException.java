/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 * Thrown to indicate that the application has attempted to parse
 * the string which does not defies the specified format.
 * 
 * @author  sahil
 * @see     util.ClientCommunicationHelper#parseLoginStatus(String)
 */
public class StringFormatException extends RuntimeException{

    /**
     * Constructs a <code>StringFormatException</code> with no detail message.
     */
    public StringFormatException() {
        this("Invalid Message Format");
    }
    
    /**
     * Constructs a <code>StringFormatException</code> with the
     * specified detail message.
     *
     * @param   s   the detail message.
     */
    public StringFormatException(String s){
        super(s);
    }
}
