/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author sahil
 */
public class MyDate {
    public static final DateFormat TIME_ONLY;
    public static final DateFormat DAY_TIME;
    public static final DateFormat DATE_TIME;
    public static final DateFormat ISO_DATE_TIME;
    public static final DateFormat DATE_YEAR;
    
    static {
        TIME_ONLY = new SimpleDateFormat("hh:mm aa");
        DAY_TIME = new SimpleDateFormat("E hh:mm aa");
        DATE_TIME = new SimpleDateFormat("dd MMM, hh:mm aa");
        ISO_DATE_TIME = new SimpleDateFormat("YYYY-MM-dd HH:mm");
        DATE_YEAR = new SimpleDateFormat("MMM dd, yyyy");
    }
    
    public static synchronized long getMSDifference(Date newDate, Date oldDate){
        return newDate.getTime() - oldDate.getTime();
    }
    
    public static synchronized String getLastSeen(Date newDate, Date oldDate){
        long diff = getMSDifference(newDate, oldDate);
        
        long hoursDiff = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
        
        if(hoursDiff <= 24){
            return "last seen today at "+TIME_ONLY.format(oldDate);
        }
        
        long dayDiff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        if(dayDiff == 1){
            return "last seen yesterday at "+TIME_ONLY.format(oldDate);
        }else if(dayDiff <= 7){
            return "last seen "+DAY_TIME.format(oldDate);
        }else if(dayDiff <= 365){
            return "last seen "+DATE_TIME.format(oldDate);
        }else{
            return "last seen "+DATE_YEAR.format(oldDate);
        }
    }
    
    public static synchronized String getCurrentDateTime(Date dateTime){
        return ISO_DATE_TIME.format(dateTime);
    }
    
    public static synchronized String msToTime(long ms){
        return TIME_ONLY.format(new Date(ms));
    }
    
    public static synchronized String getCurrentTime(){
        return TIME_ONLY.format(new Date());
    }
    
    public static void main(String[] args) throws Exception{
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        Date d = df.parse("16/May/2019");
        Date d1 = df.parse("16/May/2020");
        d = new Date();
        System.out.println(d.toString());
        System.out.println(TIME_ONLY.format(d));
        System.out.println(DAY_TIME.format(d));
        System.out.println(DATE_TIME.format(d));
//        2020-05-22 20:05
        System.out.println(ISO_DATE_TIME.format(d));
        System.out.println(DATE_YEAR.format(d));
        System.out.println(getLastSeen(d1, d));
//        System.out.println(getHourDifference(getMSDifference(d1, d)));
//        System.out.println(getYearDifference(getMSDifference(d1, d)));
    }
}
