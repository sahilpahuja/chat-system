package entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author sahil
 */
public class User {
    public final String username;
    public final String password;
    public final long creationTimeInMS;

    public User(String username, String password, long creationTimeInMS) {
        this.username = username;
        this.password = password;
        this.creationTimeInMS = creationTimeInMS;
    }
}