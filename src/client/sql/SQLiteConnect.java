/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.sql;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author sahil
 */
public class SQLiteConnect {
  /**
 * 
 * @return Connection
 * Returns a valid connection to specified DB if it can, otherwise it returns null
 */
  public static Connection getConnection(String username){
      Connection conn = null;
      try{
          String dbPath = "/home/sahil/ChatSystem/chat-system/";
          conn = DriverManager.getConnection("jdbc:sqlite:"+dbPath+username+".db");
      }catch(SQLException ex){
          JOptionPane.showMessageDialog(null, "Connection Failed: " + ex.getMessage());
      }
      return conn;
  }
  public static void main(String args[]){
      Connection con = getConnection("sahil");
  }
}