/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui;

import java.awt.Color;

/**
 *
 * @author sahil
 */
public interface Colors {
    Color BG_DARK = new Color(8, 24, 33);
    Color PRIMARY_DARK = new Color(48, 60, 66);
    Color PRIMARY_GREEN = new Color(10, 80, 71);
    Color SECONDARY_GREEN = new Color(0, 191, 165);
    Color PRIMARY_GRAY = new Color(158, 158, 158);
}
