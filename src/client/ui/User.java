/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui;

import java.awt.Color;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author sahil
 */
public final class User extends JPanel{
    public static final Color PRIMARY_GREEN_COLOR = new Color(0, 191, 165);
    public static final Border border = BorderFactory.createMatteBorder(0, 0, 1, 0, Colors.PRIMARY_DARK);
    
    private JLabel lblName;
    private JLabel lblMessageCount;
    
    private boolean online;
    private int messageCount;
    
    public User(String lblName){
        this(lblName, false);
    }
    public User(String lblName, boolean online) {
        this(lblName, online, 0);
    }
    public User(String lblName, boolean online, int messageCount) {
        init();
        
        this.lblName = new JLabel(lblName);
        
        this.lblMessageCount = new JLabel();
        this.lblMessageCount.setForeground(Colors.SECONDARY_GREEN);
        
        this.add(this.lblName);
        this.add(Box.createHorizontalGlue());
        this.add(this.lblMessageCount);
        
        setOnline(online);
        setMessageCount(this.messageCount);
    }
    
    public void setOnline(boolean online){
        this.online = online;
        this.lblName.setForeground(online?Colors.SECONDARY_GREEN:Color.WHITE);
    }
    
    public void setMessageCount(int count){
        this.messageCount = count;
        lblMessageCount.setText(count > 0? ""+count:"");
    }
    
    public int getMessageCount() {
        return this.messageCount;
    }
    
    public String getUsername(){
        return lblName.getText();
    }
    
    public boolean getOnline(){
        return this.online;
    }

    private void init() {
        this.setBackground(Colors.BG_DARK);
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        this.setBorder(border);
    }
    
    @Override
    public Insets getInsets() {
        return new Insets(20, 20, 20, 20);
    }   
    
}