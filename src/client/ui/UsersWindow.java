/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui;


//import static client.ui.Colors.BG_DARK;
import client.socket.SocketClient;
import communication.Acknowledge;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import communication.ClientCommunicationHelper;
import communication.Message;
import java.awt.event.WindowAdapter;

/**
 *
 * @author sahil
 */
public class UsersWindow extends JFrame{
    private UserList list;
    
    private final String myUsername;
    private final SocketClient socketClient;
    private final Connection conn;
    private final Map<String, ChatWindow> chatWindows;
    
    private final JPanel noInternetPanel;
    private final JPanel northPanel;
    
    private final Reconnected reconnected;
    
    public UsersWindow(SocketClient socketClient, String myUsername, Connection conn){
        this.socketClient = socketClient;
        this.myUsername = myUsername;
        this.conn = conn;
        
        this.chatWindows = new HashMap<>();
        
        this.setVisible(true);
        this.setSize(500, 800);
        initDB();
        
        noInternetPanel = new JPanel(){
            public JPanel init(){
                setLayout(new FlowLayout());
                JLabel jLabel = new JLabel("couldn't connect to server, Please check your connection.");
                jLabel.setForeground(Color.WHITE);
                jLabel.setBackground(Color.RED);
                this.setBackground(Color.RED);
                add(jLabel);
                return this;
            }
        }.init();

        northPanel = new JPanel(new MigLayout("insets 0, wrap 0", "[grow, fill]"));
        northPanel.add(createMyPanel(myUsername, Colors.PRIMARY_GREEN));
        
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(northPanel, BorderLayout.NORTH);
        
        list = new UserList(this);
        
        panel.add(list);
        this.add(panel);
        
        reconnected = new Reconnected();
        UsersWindow that = this;
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                that.setVisible(false);
                that.dispose();
                socketClient.close(that);
                System.exit(0);
            }
        });
        
        validate();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }
    
    private JPanel createMyPanel(String myUsername, Color bgColor){
        //Creating Main Panel
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new MigLayout("insets 0"));
        jPanel.setBackground(bgColor);
        jPanel.setBorder(null);
        
        //Creating myUsername JLabel
        JLabel lblUsername = new JLabel(myUsername);
        lblUsername.setFont(new Font("Courier Sans", Font.PLAIN, 20));
        lblUsername.setForeground(Color.WHITE);
        
        //Creating panel for name and last online
        JPanel currentUserPanel = new JPanel(new MigLayout("insets 0, wrap 0"));
        currentUserPanel.setBorder(new EmptyBorder(20, 20, 20, 5));
        currentUserPanel.setBackground(bgColor);
        currentUserPanel.add(lblUsername);
        
        jPanel.add(currentUserPanel);
        jPanel.add(Box.createHorizontalGlue());
        jPanel.validate();
        
        return jPanel;
    }
    
    public void addUsers(Map<String, Boolean> users){
        users.entrySet().forEach((_user) -> {
            chatWindows.put(_user.getKey(), new ChatWindow(myUsername, socketClient, _user.getKey(), conn));
        });
        list.add(users);
    }
    
    public void addUser(String username, boolean online){
        list.add(null, online, 0);
    }
    
    public void receive(Message m){
        System.out.println("Inside Users window receive, printing");
        chatWindows.entrySet().forEach((_user) -> {
            System.out.println("Chat Window: "+ _user.getKey());
        });
        ChatWindow chatWindow = chatWindows.get(m.sender);
        System.out.println("Printing sender username: " + m.sender);
        System.out.println("Chat Window: " + chatWindow);
        System.out.println("Why Null? printing message details sender:"+m.sender+" receiver" + m.receiver + "content:"+m.content);
        if(chatWindow != null){
            System.out.println("Chat window is not null");
            chatWindow.receive(m);
            if(!chatWindow.isVisible()) {
                list.incrementMessageCount(m.sender);
            }
        }
    }
    
    public void receive(Acknowledge ack){
        ChatWindow chatWindow = chatWindows.get(ack.sender);    
        if(chatWindow != null){
            chatWindow.receive(ack);
        }
    }
    
    public void setOnline(String username, boolean online){
        // In case when 
        if (myUsername.equals(username)) {
            return;
        }
        // Change UserPanel Color
        list.setOnline(username, online);
        // Change 
        ChatWindow chatWindow = chatWindows.get(username);
        if (chatWindow.isVisible()) {
            if (online) {
                chatWindow.setLastSeen("online");
            } else {
                chatWindow.setLastSeen("");
                socketClient.send(ClientCommunicationHelper.createGetLastSeenString(username), true);
            }
        }
    }
    
    public void showUser(String username){
        ChatWindow chatWindow = chatWindows.get(username);
        if(chatWindow != null){
            chatWindow.setVisible(true);
        }else{
            chatWindow = new ChatWindow(myUsername, socketClient, username, conn);
            chatWindows.put(username, chatWindow);
        }
        if(list.get(username).getOnline()){
            chatWindow.setLastSeen("online");
        }else{
            chatWindow.setLastSeen("");
            socketClient.send(ClientCommunicationHelper.createGetLastSeenString(username), true);
        }
    }
    
    public void showFailedConnection(boolean flag){
        if(flag && noInternetPanel.getParent() != northPanel){
            northPanel.add(noInternetPanel, "dock north");
        }else if(!flag && noInternetPanel.getParent() == northPanel){
            northPanel.remove(noInternetPanel);
            reconnected.start();
        }
        revalidate();
    }
    
    public void setLastSeen(String username, String lastSeen){
        ChatWindow chatWindow = chatWindows.get(username);
        if(chatWindow != null && chatWindow.isVisible()){
            chatWindow.setLastSeen(lastSeen);
        }
    }
    
    public static void main(String[] args) {
    }

    private void initDB() {
        try {
            System.out.println("Table created: "+conn.createStatement().execute("CREATE TABLE IF NOT EXISTS messages (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, message TEXT, status SMALLINT, username VARCHAR(30), time_ms INTEGER)"));
        } catch (SQLException ex) {
            Logger.getLogger(UsersWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private class Reconnected implements Runnable{
        private Thread thread;
        @Override
        public void run(){
            for(Map.Entry<String, ChatWindow> entry : chatWindows.entrySet()){
                ((ChatWindow)entry.getValue()).onReconnected();
            }
        }
        public void start(){
            if(thread == null){
                thread = new Thread(this);
            }
            if(!thread.isAlive()){
                thread = new Thread(this);
                thread.start();
            }
        }
    }
}
