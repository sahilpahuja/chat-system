/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.image.ImageObserver;
import java.text.AttributedCharacterIterator;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import net.miginfocom.swing.MigLayout;
import org.davidmoten.text.utils.WordWrap;

/**
 *
 * @author sahil
 */
public class MessagePanel extends JPanel{
    
    static final EmptyBorder PADDING_BORDER;
    
    public static final Color TEXT_COLOR;
    public static final Color TIME_COLOR;
    public static final Color SEND_COLOR;
    public static final Color RECEIVE_COLOR;
    
    public static final ImageIcon PENDING_ICON;
    public static final ImageIcon SENT_ICON;
    public static final ImageIcon DELIVERED_ICON;
    public static final ImageIcon SEEN_ICON;
    
    //Message Status
    /**
     * Indicates message is pending to be sent on client side
     * and for server-side ACK:SERVER_RECEIVED is pending to be send to client
    **/
    public static final int SENT_PENDING = 0;
    /**
     * Indicates that message is sent till server(single tick)
     * and for server-side ACK:SERVER_RECEIVED is successfully sent to client
    **/
    public static final int SENT_SERVER = 1;
    public static final int SENT_DELIVERED = 2;
    public static final int SENT_SEEN = 3;
    
    public static final int RECEIVED_RECEIVED = 4;
    public static final int RECEIVED_SEEN = 5;
    public static final int RECEIVED_NO_ACK = 6;
    public static final int RECEIVED_SEEN_NO_ACK = 7;
    
    public static final boolean SEND = true;
    public static final boolean RECEIVE = false;
    
    private final boolean messageType;
    private String text;
    private String time;
    private JLabel lblDeliveryStatus;

    static {
        PADDING_BORDER = new EmptyBorder(new Insets(10, 10, 10, 10));
        
        TEXT_COLOR = new Color(238, 238, 238);
        TIME_COLOR = new Color(158, 158, 158);
        SEND_COLOR = new Color(10, 80, 71);
        RECEIVE_COLOR = new Color(48, 60, 66);
        
        PENDING_ICON = new ImageIcon("/home/sahil/ChatSystem/chat-system/src/client/ui/images/time.png");
        SENT_ICON = new ImageIcon("/home/sahil/ChatSystem/chat-system/src/client/ui/images/tick.png");
        DELIVERED_ICON = new ImageIcon("/home/sahil/ChatSystem/chat-system/src/client/ui/images/double-tick.png");
        SEEN_ICON = new ImageIcon("/home/sahil/ChatSystem/chat-system/src/client/ui/images/blue-tick.png");
    }
    public MessagePanel(String text, boolean messageType, String time, int status) {
        this.messageType = messageType;
        this.text = text;
        this.time = time;
        
        setOpaque(false);
        lblDeliveryStatus = new JLabel();
        
        setStatus(status);
        
        init();
        addComponents();
    }

    private void init() {
        setBorder(PADDING_BORDER);
        setBackground(messageType?SEND_COLOR:RECEIVE_COLOR);
        setLayout(new MigLayout("insets 10"));
    }
    
    private void addComponents() {
        
        //Adding main message
        int firstSpace = text.indexOf(" ");
        String wrappedText = WordWrap.from(text).newLine("<br>").maxWidth(firstSpace>24||firstSpace < 0?25:38).wrap();
        JLabel lblText = new JLabel("<html>"+wrappedText+"</html>");
        lblText.setForeground(TEXT_COLOR);
        add(lblText, "dock west");
        
        //Adding time and delivery status
        JPanel deliveryInfo = new JPanel(new MigLayout("insets 0"));
        deliveryInfo.setOpaque(true);
        deliveryInfo.setBackground(new Color(0,0,0,0));
        
        JLabel lblTime = new JLabel(time);
        lblTime.setForeground(TIME_COLOR);
        
        deliveryInfo.add(lblTime);
        deliveryInfo.add(lblDeliveryStatus);
        
        
        add(deliveryInfo,"gapleft 10, dock south");
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Dimension arcs = new Dimension(25,25);
        int width = getWidth();
        int height = getHeight();
        Graphics2D graphics = (Graphics2D) g;
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);


        //Draws the rounded opaque panel with borders.
        graphics.setColor(getBackground());
        graphics.fillRoundRect(0, 0, width-1, height-1, arcs.width, arcs.height);//paint background
        graphics.setColor(getForeground());
        graphics.drawRoundRect(0, 0, width-1, height-1, arcs.width, arcs.height);//paint border
    }
    
    public void setStatus(int status){
        ImageIcon icon = null;
        switch (status){
            case SENT_SERVER: 
                icon = SENT_ICON;
                break;
            case SENT_DELIVERED:
                icon = DELIVERED_ICON;
                break;
            case SENT_SEEN:
                icon = SEEN_ICON;
                break;
            case SENT_PENDING:
                icon = PENDING_ICON;
                break;
        }
        
        lblDeliveryStatus.setIcon(icon);
        repaint();
    }
}
