/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui;

import client.socket.SocketClient;
//import static client.ui.Tp.SEND_ICON;
import communication.Acknowledge;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import net.miginfocom.swing.MigLayout;
import communication.ClientCommunicationHelper;
import communication.Message;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.HashMap;
import java.util.Map;
import util.MyDate;

/**
 *
 * @author sahil
 */
public class ChatWindow extends JFrame{
    private final JTextArea messageField;
    private final JPanel messagePane;
    private final JScrollPane scrollPane;
    private final JScrollBar verticalBar;
    private final SocketClient socketClient;
    private final String myUsername;
    private final String username;
    private final Connection conn;
    private final JLabel lastSeen;
    
    private final Map<Long, MessagePanel> messagesNotSeenByReceiver;
    
    public String getUsername()
    {
        return this.username;
    }
    
    public ChatWindow(String myUsername, SocketClient socketClient, String username, Connection conn){
        setMinimumSize(new Dimension(500, 800));
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                onShow();
            }
        });
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        this.myUsername = myUsername;
        this.socketClient = socketClient;
        this.username = username;
        this.conn = conn;
        
        messagesNotSeenByReceiver = new HashMap<>();
        
        messagePane = new JPanel();
        scrollPane = new JScrollPane();
        // Storing scrollbar for scrolling manipulations
        verticalBar = scrollPane.getVerticalScrollBar();
        messageField = new MessageField(400, 10);
        lastSeen = new JLabel(" ");
        lastSeen.setForeground(Colors.PRIMARY_GRAY);
        
        addComponents();
        loadMessages();
    }
    
    private void addComponents(){
        // The pane on which messages will be added
        messagePane.setBorder(new EmptyBorder(10, 10, 10, 10));
        messagePane.setLayout(new MigLayout(
        // set the automatic wrap after columns
        "insets 0, wrap 0", 
        // hardcode fixed column width and fixed column gap 
        "", 
        // hardcode fixed height and a zero row gap
        "[]10"));
        messagePane.setBackground(Colors.BG_DARK);
        
        /* This panel is cover for message pane which will shrink
        the width of messagePane to BorderLayout.PAGE_START when 
        there are less messages */
        JPanel messagePaneCover = new JPanel(new BorderLayout());
        messagePaneCover.setBackground(new Color(8, 24, 33));
        messagePaneCover.add(messagePane, BorderLayout.PAGE_START);
        
        // Scrollpane on which messagePaneCover will be added
        scrollPane.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBorder(null);
        scrollPane.getViewport().setView(messagePaneCover);
        
        JPanel bottomPane = new JPanel(new MigLayout("insets 0", "[grow, fill]rel:push[]"));
        bottomPane.setBackground(new Color(8, 24, 33));
        bottomPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        
        messageField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent ke){
                if(ke.getModifiers() == KeyEvent.CTRL_MASK && ke.getKeyCode() == KeyEvent.VK_ENTER){
                    send(messageField.getText().trim());
                    messageField.setText("");
                }
            }
        });
        bottomPane.add(messageField);
        
//        JButton sendButton = new JButton(SEND_ICON);
        JButton sendButton = new JButton("S");
        sendButton.setFocusPainted(false);
        sendButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
        sendButton.setContentAreaFilled(false);
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                send(messageField.getText().trim());
                messageField.setText("");
            }
        });
        bottomPane.add(sendButton, "gapleft 10");
        
        // Adding user's top panel
        add(createCurrentUserPanel(), BorderLayout.NORTH);
        
        // Adding scrollpane to display messages
        add(scrollPane);
        
        // Adding panel which contains messageField and Send Button
        add(bottomPane, BorderLayout.SOUTH);
        
//        demoMessages();
        validate();
        verticalBar.setUnitIncrement(20);
        verticalBar.setValue(verticalBar.getMaximum());
    }
    
    private JPanel createCurrentUserPanel(){
        return new JPanel(){
            public JPanel initialize(){
                setLayout(new MigLayout("insets 10, wrap 0"));
                setBackground(Colors.BG_DARK);
                setBorder(new LineBorder(Colors.PRIMARY_DARK));
                JLabel label = new JLabel(username);
                label.setFont(new Font("Courier Sans", Font.PLAIN, 20));
                label.setForeground(Color.WHITE);
                add(label);
                
                add(lastSeen);
                return this;
            }
            @Override
            public Insets getInsets() {
                return new Insets(10, 10, 10, 10);
            }   
        }.initialize();
    }
    
    private void loadMessages(){
        try {
            PreparedStatement ps = conn.prepareStatement("SELECT message, status, time_ms FROM messages WHERE username = ? ORDER BY id");
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                addMessage(rs.getString("message"), rs.getInt("status"), rs.getLong("time_ms"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void addMessage(String message, int status, long creationMs){
        //status > 3 is to check if message is received message
        MessagePanel m = new MessagePanel(message, status>3?MessagePanel.RECEIVE:MessagePanel.SEND, MyDate.msToTime(creationMs), status);
        if(status > 3){
            messagePane.add(m, "width ::420");
        }else{
            messagesNotSeenByReceiver.put(creationMs, m);
            messagePane.add(m, "push, al right, width ::420");
        }
        revalidate();
        
        
        int max = verticalBar.getMaximum();
        int pos = verticalBar.getValue() + verticalBar.getVisibleAmount();
        
        // 75 is the minimum percentage at which scroll will be set to 100% after receiving or sending a message
        if(((pos*100)/max) > 75){
            verticalBar.setValue(max);
        }
    }

    public void send(String message){
        long currentTimeInMs = System.currentTimeMillis();
        try {
            PreparedStatement ps = conn.prepareStatement("INSERT INTO messages (message, status, username, time_ms) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            System.out.println("Added message in DB");
            ps.setString(1, message);
            ps.setInt(2, MessagePanel.SENT_PENDING);
            ps.setString(3, username);
            ps.setLong(4, currentTimeInMs);
            ps.execute();
            addMessage(message, 0, currentTimeInMs);
            System.out.println("Added "+currentTimeInMs+" message in HM");
            ResultSet rs = ps.getGeneratedKeys();
            int generatedKey = 0;
            if (rs.next()) {
                generatedKey = rs.getInt(1);
            }
            socketClient.send(ClientCommunicationHelper.createMessageString(new Message(this.myUsername, message, username, currentTimeInMs)), true);
            
        } catch (SQLException ex) {
            Logger.getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void receive(Message m){
        try {
            synchronized(conn){
                PreparedStatement ps = conn.prepareStatement("INSERT INTO messages (message, status, username, time_ms) VALUES (?, ?, ?, ?)");
                ps.setString(1, m.content);
                ps.setInt(2, MessagePanel.RECEIVED_NO_ACK);
                ps.setString(3, username);
                ps.setLong(4, m.creationTimeInMS);
                ps.execute();
                addMessage(m.content, MessagePanel.RECEIVED_NO_ACK, m.creationTimeInMS);
                revalidate();

                boolean visible = this.isVisible();
                int status = visible?Acknowledge.CLIENT_SEEN:Acknowledge.CLIENT_RECEIVED;
                boolean ackSentSuccessfully = socketClient.send(ClientCommunicationHelper.createAckString(new Acknowledge(status, m.receiver, m.sender, m.creationTimeInMS)), false);

                /**
                 * Acknowledge.CLIENT_RECEIVED value(4) is same as MessagePanel.RECEIVED_RECEIVED
                 * Acknowledge.CLIENT_SEEN value(5) is same as MessagePanel.RECEIVED_SEEN
                 * Status will only be modified if acknowledge is not sent
                 */
//                String whereStatusQuery = "current received(4) or pending(6) walo ka atleast received(4)...... current seen(5) aur pending(6/7) walo ka atleast seen(7)";
                String whereStatusQuery = "";
                if(ackSentSuccessfully){
                    if(visible){
                        // status is already RECEIVED_SEEN (set above)
                        whereStatusQuery = "status IN ("+MessagePanel.RECEIVED_RECEIVED+","+MessagePanel.RECEIVED_NO_ACK+","+MessagePanel.RECEIVED_SEEN_NO_ACK+")";
                    }else{
                        // status is already RECEIVED_RECEIVED (set above)
                        whereStatusQuery = "status = "+MessagePanel.RECEIVED_NO_ACK;
                    }
                }else{
                    if(visible){
                        status = MessagePanel.RECEIVED_SEEN_NO_ACK;
                        whereStatusQuery = "status IN ("+MessagePanel.RECEIVED_RECEIVED+","+MessagePanel.RECEIVED_NO_ACK+")";
                    }else{
                        //No need to update because status is already RECEIVED_NO_ACK in database
                        status = -1;
                    }
                }

                if(status != -1){
                    conn.createStatement().executeUpdate("UPDATE messages SET status="+status+" WHERE username = '"+username+"' AND "+whereStatusQuery);
                }
                System.out.println("Ack Sent if connection is establised, Status = "+status);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void receive(Acknowledge ack){
        try {
            synchronized(conn){
                /**
                 * Acknowledge.SERVER_RECEIVED value(1) is same as MessagePanel.SENT_SERVER
                 * Acknowledge.RECEIVER_DELIVERED value(2) is same as MessagePanel.SENT_DELIVERED
                 * Acknowledge.RECEIVER_SEEN value(3) is same as MessagePanel.SENT_SEEN
                 */
                String statusQuery = null;
                int status=0;
                switch(ack.ackType){
                    case Acknowledge.SERVER_RECEIVED:
                        status = MessagePanel.SENT_SERVER;
                        statusQuery = "status="+MessagePanel.SENT_PENDING;
                        break;
                    case Acknowledge.RECEIVER_DELIVERED:
                        status = MessagePanel.SENT_DELIVERED;
                        statusQuery = "status IN ("+MessagePanel.SENT_PENDING+","+MessagePanel.SENT_SERVER+")";
                        break;
                    case Acknowledge.RECEIVER_SEEN:
                        status = MessagePanel.SENT_SEEN;
                        statusQuery = "status IN ("+MessagePanel.SENT_PENDING+","+MessagePanel.SENT_SERVER+","+MessagePanel.SENT_DELIVERED+")";
                        break;
                }

                ResultSet rs = conn.createStatement().executeQuery("SELECT time_ms, message FROM messages WHERE username = '"+username+"' AND time_ms <= " + ack.creationTimeInMS + " AND " + statusQuery);
                while(rs.next()){
                    ((MessagePanel)messagesNotSeenByReceiver.get(rs.getLong(1))).setStatus(ack.ackType);
                    
                    if(ack.ackType == Acknowledge.RECEIVER_SEEN){
                        messagesNotSeenByReceiver.remove(ack.creationTimeInMS);
                    }
                }
                
                conn.createStatement().executeUpdate("UPDATE messages SET status="+status+" WHERE username = '"+username+"' AND time_ms <= " + ack.creationTimeInMS + " AND " + statusQuery);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setLastSeen(String lastSeen){
        this.lastSeen.setText(lastSeen);
    }
    
    public void onShow(){
        try {
            synchronized(conn){
//                String whereStatusQuery = "status IN ("+MessagePanel.RECEIVED_RECEIVED+","+MessagePanel.RECEIVED_NO_ACK+","+MessagePanel.RECEIVED_SEEN_NO_ACK+")";
//                ResultSet rs = conn.createStatement().executeQuery("SELECT status, time_ms FROM messages WHERE username = '"+username+"' AND status = "+MessagePanel.RECEIVED_RECEIVED + " OR status = "+MessagePanel.RECEIVED_SEEN+" ORDER BY id");
                
                ResultSet rs = conn.createStatement().executeQuery("SELECT status, time_ms FROM messages WHERE username = '"+username+"' AND status <> "+MessagePanel.RECEIVED_SEEN+" ORDER BY id");
                
                while(rs.next()){
                    int status = rs.getInt(1);
                    long time = rs.getLong(2);
                    boolean ackSentSuccessfully = socketClient.send(ClientCommunicationHelper.createAckString(new Acknowledge(Acknowledge.CLIENT_SEEN, myUsername, username, time)), false);
                    if(ackSentSuccessfully){
                        status = MessagePanel.RECEIVED_SEEN;
                    }else if(status == MessagePanel.RECEIVED_RECEIVED || status == MessagePanel.RECEIVED_NO_ACK){
                        status = MessagePanel.RECEIVED_SEEN_NO_ACK;
                    }
                    conn.createStatement().executeUpdate("UPDATE messages SET username = '"+username+"' AND status="+status+" WHERE time_ms = " + time + " AND status > 3");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onReconnected(){
//        try {
//            synchronized(conn){
//                ResultSet rs = conn.createStatement().executeQuery("SELECT status, time_ms FROM messages WHERE username = '"+username+"' AND status = " + MessagePanel.RECEIVED_PENDING + " OR status = "+MessagePanel.RECEIVED_SEEN+" ORDER BY id");
//                
//                int status = rs.getInt(1);
//                long time = rs.getLong(2);
//                
//                while(rs.next()){
//                    boolean ackSentSuccessfully = socketClient.send(ClientCommunicationHelper.createAckString(new Acknowledge(status==MessagePanel.RECEIVED_PENDING?Acknowledge.ACK_RECEIVED:Acknowledge.ACK_SEEN, username, myUsername, time)), false);
//                    if(ackSentSuccessfully){
//                        if(status==MessagePanel.RECEIVED_PENDING){
//                            status = MessagePanel.RECEIVED_RECEIVED;
//                        }else{
//                            status = MessagePanel.RECEIVED_SEEN_ACK;
//                        }
//                    }
//                    conn.createStatement().executeQuery("UPDATE messages SET status="+status+" WHERE username = '"+username+"' AND time_ms = " + time + " AND status > 3");
//                }
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
    
    public static void main(String[] args) {
        new ChatWindow(null, null, null, null).demoMessages();
    }
    
    private void demoMessages(){
        messagePane.add(new MessagePanel(" NETFLIX ORIGINALS 4K UHD💥\n" +
"                  PREMIUM PLAN\n" +
"\n" +
"〽️ Official Price 7̶9̶9̶ ₹̶\n" +
"〽️ Our Price 149₹ flat/-\n" +
"     {Best Rates are BACK}\n" +
"\n" +
"💢 Screens - 4⃣\n" +
"💢 Private - Yes\n" +
"💢 Password Changeable - Yes \n" +
"💢 Validity - 1 Month\n" +
"💢 Guarantee - 1 Month\n" +
"💢 TV/Stick Support - Yes\n" +
"\n" +
"\n" +
"Resellers Buying 3+ Accounts in One Go will be Offered  much less prices💯💯\n" +
"\n" +
".............. Premium_Stuff™ ..............\n" +
"        (Name Enough For Trust)", MessagePanel.RECEIVE, "6:40 pm", -1), "width ::420");
        messagePane.add(new MessagePanel("Ni kia yar abtak series dekh rha hun", MessagePanel.SEND, "6:45 pm", 3), "push, al right, width ::420");
        messagePane.add(new MessagePanel("https://stackoverflow.com/q/61727430/12035932", MessagePanel.RECEIVE, "6:45 pm", -1), "width ::420, wrap");
        messagePane.add(new MessagePanel("Jhon", MessagePanel.SEND, "6:46 pm", 2), "push, al right, width ::420");
        messagePane.add(new MessagePanel("lorem ipsum dollar ishbu lorem ipsum dollar ishbu", MessagePanel.SEND, "6:46 pm", 1), "push, al right, width ::420");
        messagePane.add(new MessagePanel("wwwwwwwwwwwwwwwwwwwwwwwww wwwwwwwwwwwwwwwwwwwwwwwww wwwwwwwwwwwwwwwwwwwwwwwww", MessagePanel.SEND, "6:47 pm", 0), "push, al right,width ::420");
        messagePane.add(new MessagePanel("encyclopedia", MessagePanel.RECEIVE, "6:47 pm", -1), "width ::420, wrap");
        messagePane.add(new MessagePanel("Bol mein ni krra hun chat sys phone krna band kr dega😂", MessagePanel.RECEIVE, "6:50 pm", -1), "width ::420, wrap");
        revalidate();
    }
}
