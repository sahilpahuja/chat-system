/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 *
 * @author sahil
 */
public class MessageField extends JTextArea{
    static final EmptyBorder PADDING_BORDER;
    public static final Color BACKGROUND;
    
    static {
        PADDING_BORDER = new EmptyBorder(new Insets(10, 10, 10, 10));
        BACKGROUND = new Color(58, 70, 76);
    }
    
    public MessageField(int width, int height) {
        setLineWrap(true);
        setCaretColor(Color.WHITE);
        setBorder(PADDING_BORDER);
        setMinimumSize(new Dimension(width, height));
        setBackground(BACKGROUND);
        setForeground(Color.WHITE);
    }
    
}
