/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author sahil
 */
public class UserList extends JScrollPane implements MouseListener{
    private final JPanel innerPanel;
    private final Map<String, User> users;
    private final MigLayout layout;
    private final UsersWindow usersWindow;
    
    public UserList(UsersWindow usersWindow){
        this.usersWindow = usersWindow;
        
        setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
        setMinimumSize(new Dimension(500, 500));
        setBackground(Colors.BG_DARK);
        
        innerPanel = new JPanel();
        layout = new MigLayout(
        // set the automatic wrap after columns
        "insets 0, wrap " + 0, 
        // hardcode fixed column width and fixed column gap 
        "[grow, fill]", 
        // hardcode fixed height and a zero row gap
        "[fill]0");
        innerPanel.setLayout(layout);
        innerPanel.setBackground(Colors.BG_DARK);
        
        getViewport().setView(innerPanel);
        
        users = new HashMap<>();
    }
    
    public void add(Map<String, Boolean> users){
        for(Map.Entry<String, Boolean> entry : users.entrySet()){
            System.out.println("Adding: " + entry.getValue());
            add(entry.getKey(), entry.getValue(), 0);
        }
    }
        
    public boolean add(String userName, boolean online, int count){
        if(users.containsKey(userName)){
            return false;
        }
        User user = new User(userName, online, count);
        users.put(userName, user);
        user.addMouseListener(this);
        innerPanel.add(user, "width 200::");
        validate();
        return true;
    }
    
    public void setOnline(String username, boolean online){
        ((User)users.get(username)).setOnline(online);
    }
    
    public void incrementMessageCount(String username){
        User user = ((User)users.get(username));
        user.setMessageCount(user.getMessageCount()+1);
    }
    
    public User get(String userName){
        return users.get(userName);
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        ((User)e.getSource()).setBackground(Colors.PRIMARY_DARK);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        User user = (User)e.getSource();
        user.setBackground(Colors.BG_DARK);
        usersWindow.showUser(user.getUsername());
        user.setMessageCount(0);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
