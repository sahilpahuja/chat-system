
package client.socket;
import communication.Acknowledge;
import communication.Message;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import communication.ServerCommunicationHelper;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Echoer extends ServerCommunicationHelper implements Runnable{
	Socket socket;
        BufferedReader input;
        Random rand;
	Echoer(Socket socket){
                new Thread(this).start();
		this.socket = socket;
                
                rand = new Random();
	}

	@Override
	public void run(){
		try{
	    	input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	    	PrintWriter output = new PrintWriter(socket.getOutputStream(), true);

	   		while (true) {
	   			String echoString = input.readLine();
                                if(ServerCommunicationHelper.parseMessageType(echoString) == ServerCommunicationHelper.LAST_SEEN){
                                    System.out.println("LAST SEEN request");
                                    String username = removeMessageType(echoString);
                                    Date d = new Date(new Date().getTime()-60000);
                                    output.println(createLastSeenString(username, d));
                                }
                                else{
                                    System.out.println("Client Sent: " + echoString);
                                    int messagetype;
                                    System.out.println("Message Type: " + (messagetype = parseMessageType(echoString)));
                                    String incomingPacket = removeMessageType(echoString);
                                    if(messagetype == LOGIN){
                                        String loginDetails[] = incomingPacket.split(DELIMITER);
                                        output.println(createLoginResultString(loginDetails[0].equals("sahil") && loginDetails[1].equals("sahil")));
                                        HashMap<String, Boolean> users = new HashMap<>();
                                        users.put("sahil", true);
                                        users.put("mihir", true);
                                        users.put("chirag", false);
                                        users.put("jaanvi", false);
                                        users.put("aryan", true);
                                        output.println(createUsersString(users));
                                    }else if(messagetype == SIGNUP){
                                        String signupDetails[] = incomingPacket.split(DELIMITER);
                                        System.out.println("Signining up....");
                                        output.println(createSignupResultString(true));
                                    }else if(messagetype == MESSAGE){
                                        System.out.println("Incoming Packet: "+incomingPacket);
                                        Message m = parseMessageString(incomingPacket);
                                        System.out.println("Message received");
                                        try {
                                            Thread.sleep(3000);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(Echoer.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        output.println(createMessageString(new Message(m.receiver, m.content, m.sender, System.currentTimeMillis())));
                                        System.out.println("Sending ACK");
                                        String ack = ServerCommunicationHelper.createAckString(new Acknowledge(Acknowledge.SERVER_RECEIVED, m.sender, m.receiver, m.creationTimeInMS));
                                        output.println(ack);
                                        int min = 2;
                                        int max = 3;
                                        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);
                                        ack = ServerCommunicationHelper.createAckString(new Acknowledge(random_int, m.sender, m.receiver, m.creationTimeInMS));
                                        output.println(ack);
                                    }else if(messagetype == ACK){
                                        System.out.println("Ack received");
                                        System.out.println("Ack type: " + parseAckString(incomingPacket).ackType);
                                    }
                                }
	   		}
	   	}catch(IOException e){
	   		System.out.println("Exception in Echoer : " + e);
	   	}finally{
	   		try{
	   			socket.close();
	   		}catch(IOException e){
	   			//WRITE CODE TO HANDLE EXCEPTION IF OCCURED
	   		}
	   	}
	}
        
        private String readLine(){
            try {

                long time = System.currentTimeMillis();
                while(!input.ready()){
                    if(System.currentTimeMillis() - time > 10000){
                        if(!socket.getInetAddress().isReachable(10000)){
                            return null;
                        }
                        time = System.currentTimeMillis();
                    }
                }
                return input.readLine();
            } catch (IOException ex) {
                ;
            }finally{
                return null;
            }
    }
}