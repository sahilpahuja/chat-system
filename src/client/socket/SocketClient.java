/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.socket;

import client.sql.SQLiteConnect;
import client.ui.Login;
import client.ui.UsersWindow;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import communication.ClientCommunicationHelper;
import static communication.ClientCommunicationHelper.parseLoginStatus;
import static communication.ClientCommunicationHelper.parseUserList;
import communication.CommunicationHelper;
import static communication.CommunicationHelper.removeMessageType;
import java.awt.event.WindowAdapter;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.CustomQueue;

/**
 *
 * @author sahil
 */
public class SocketClient extends ClientCommunicationHelper{
    private static SocketClient socketClient;
    private static Login login;
    private static UsersWindow usersWindow;
    
    private final Socket socket;
    private PrintWriter output;
    private BufferedReader input;
    private Reader inputReader;
    private InetAddress serverAddress;
    private CustomQueue<String> messageQueue;
    /**
     * Creates a stream socket and connects it to the specified port number of host address.
     * <p>
     * If the specified host is {@code null} it is the equivalent of
     * specifying the address as
     * {@link java.net.InetAddress#getByName InetAddress.getByName}{@code (null)}.
     * In other words, it is equivalent to specifying an address of the
     * loopback interface. </p>
     * <P>
     * @param      host   the host name, or {@code null} for the loopback address.
     * @param      port   the port number.
     * 
     * @exception  UnknownHostException if the IP address of
     *             the host could not be determined.
     * @exception  IOException  if an I/O error occurs when creating the socket.
     * @exception  SecurityException  if a security manager exists and its
     *             {@code checkConnect} method doesn't allow the operation.
     * @exception  IllegalArgumentException if the port parameter is outside
     *             the specified range of valid port values, which is between
     *             0 and 65535, inclusive.
     */
    public SocketClient(String host, int port) throws IOException{
        socket = new Socket(host, port);
        initStreams(socket);
        this.serverAddress = socket.getInetAddress();
        messageQueue = new CustomQueue<>(output, serverAddress, this);
        startInputRead();
    }
    private void initStreams(Socket socket) throws IOException{
        output = new PrintWriter(socket.getOutputStream(), true);
        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }
    private void startInputRead(){
        inputReader = new Reader();
        inputReader.startReading();
    }
    
    public synchronized boolean send(String message, boolean enque){
        if(messageQueue.isRunning() && enque){
            messageQueue.add(message);
        }else{
            output.println(message);
            try{
                boolean reach = serverAddress.isReachable(1000);
                if(!reach && enque){
                    messageQueue.add(message);
                    return false;
                }else{
                    return reach;
                }
            }catch(IOException ioe){
                System.out.println("Exception: " + ioe);
            }
        }
        return false;
    }
    
    public void login(String username, String password){
        send(createLoginString(username, password), true);
    }
    
    
    public void signup(String username, String password){
        send(createSignupString(username, password), true);
    }
    
    public static SocketClient connect(Login context, String host, int port) throws IOException {
        if(context == login){
            if(socketClient == null){
                socketClient = new SocketClient(host, port);
            }
            return socketClient;
        }
        return null;
    }
    
    public void onLoginSuccessful(Login context){
        if(context == login){
            String username = login.getUsername();
            login.dispose();
            usersWindow = new UsersWindow(this, username, SQLiteConnect.getConnection(username));
        }
    }
    
    public void setConnection(boolean connected){
        usersWindow.showFailedConnection(!connected);
    }
    
    public void close(UsersWindow context){
        if(context == this.usersWindow){
            System.out.println("Called close");
            inputReader.stopReading();
            try {
                input.close();
                output.close();
                socket.close();
            } catch (IOException ex) {
                Logger.getLogger(SocketClient.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
    public static void main(String[] args) {
        login = new Login();
    }
    
    private class Reader implements Runnable{
        private volatile boolean status;
        private Thread reader;
        public Reader() {
            reader = new Thread(this);
        }
        @Override
        public void run(){
            try{
                while(status){
                    while(!input.ready() && status){
                        Thread.sleep(1000);
                    }
                    if(status){
                        String incomingPacket = input.readLine();
                        if ((HEARTBEAT+"").equals(incomingPacket)) {
                            output.println(HEARTBEAT);
                        }
                        System.out.println("Incoming on CLient: " + incomingPacket);
                        int messageType = parseMessageType(incomingPacket);
                        String messageWithoutType = removeMessageType(incomingPacket);
                        switch(messageType){
                            case ClientCommunicationHelper.MESSAGE:
                                usersWindow.receive(parseMessageString(messageWithoutType));
                                break;
                            case ClientCommunicationHelper.USER_STATUS:
                                String[] userStatus = CommunicationHelper.breakInStrings(messageWithoutType, 2);
                                usersWindow.setOnline(userStatus[0], readStringIntBool(userStatus[1]));
                                break;
                            case ClientCommunicationHelper.LOGIN:
                                boolean loginStatus = parseLoginStatus(messageWithoutType);
                                login.loginStatus(loginStatus);
                                break;
                            case ClientCommunicationHelper.SIGNUP:
                                boolean signupStatus = parseSignupStatus(messageWithoutType);
                                login.signupStatus(signupStatus);
                                break;
                            case ClientCommunicationHelper.USER_LIST:
                                // Wait until memory assignment of users window
                                Map<String, Boolean> users = parseUserList(messageWithoutType);
                                usersWindow.addUsers(users);
                                break;
                            case ClientCommunicationHelper.LAST_SEEN:
                                String[] lastSeen = CommunicationHelper.breakInStrings(messageWithoutType, 2);
                                usersWindow.setLastSeen(lastSeen[0], parseLastSeenValue(lastSeen[1]));
                                break;
                            case ClientCommunicationHelper.ACK:
                                usersWindow.receive(parseAckString(messageWithoutType));
                                break;
                        }
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SocketClient.class.getName()).log(Level.SEVERE, null, ex);
            } catch(IOException e){
                System.out.println("Exception while reading input from server: "+e);
            }
        }
        
        public void startReading(){
            status = true;
            reader.start();
        }
        
        public void stopReading(){
            status = false;
        }
    }
}
